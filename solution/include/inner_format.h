#ifndef INNER_FORMAT
#define INNER_FORMAT

#include <stdlib.h>
#include <stdint.h>

struct pixel { uint8_t b,g,r; };
struct image {
 uint64_t width, height;
 struct pixel* data;
};

struct image image_create(uint64_t width, uint64_t height, struct pixel* data);

struct image image_create_empty();

struct pixel* image_get_data(struct image const* img);

uint64_t image_get_data_size(struct image const* img);

uint64_t image_get_width(struct image const* img);

uint64_t image_get_height(struct image const* img);

void image_set_width(uint64_t width, struct image* img);

void image_set_height(uint64_t height, struct image* img);

void image_set_data(struct pixel* data, struct image* img);

void free_data(struct image* img);
#endif
