#ifndef FILE_SERVICE
#define FILE_SERVICE

#include <stdio.h>

enum open_status {

	OPEN_OK = 0,
	OPEN_ERR

};

enum open_status open_file_for_reading(char* file_path, FILE** file);

enum open_status open_file_for_writing(char* file_path, FILE** file);

#endif
